
class Operations {
    Integer acumulador
    def sum() {
        acumulador=0;
        {a,b -> a+b}
    }

    def prod() {
        acumulador=1;
        {a,b -> a*b}
    }

    def methodMissing(String name, args) {
        println "No se encuentra implementada la operacion requerida."
    }
}

class Test {
    static visitor(g, List list, acumulador) {
        list.inject(acumulador) { total, numero ->
            g(total, Integer.parseInt(numero))
        }
    }

    static void main (args) {
        if (args.size() == 2) {
            List list = args[0].substring(1,args[0].size()-1).split(",")
            def op = new Operations()
            def operacion = op."${args[1]}"()

            try {
                println visitor(operacion, list, op.acumulador)
            } catch(Exception e) {
                //aca se debe manejar correctamente la excepcion
                println "Ocurrio una excepcion"
            }
        } else {
            println "Argumentos mal enviados. Lista y metodo"
        }
    }
}
