/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test

def name='w7'

class LanguageList {
    def list = ['Java', 'Groovy', 'Scala']
 
    // Set metaClass property to ExpandoMetaClass instance, so we
    // can add dynamic methods.
    LanguageList() {
        def mc = new ExpandoMetaClass(LanguageList, false, true)
        mc.initialize()
        this.metaClass = mc
    }
     
    def methodMissing(String name, args) {
        // Intercept method that starts with find.
        if (name.startsWith("find")) {
            def result = list.find { it == name[4..-1] }
            // Add new method to class with metaClass.
            LanguageList.metaClass."$name" = {-> result + "[instancia]" }
            this.metaClass."$name" = {-> result + "[cache]" }
            result
        } else {
            throw new MissingMethodException(name, this.class, args)
        }
    }
}

println "Hello $name!"

def languages = new LanguageList()
println languages.findGroovy()
println languages.findScala()
println languages.findJava()
println languages.findRuby()

println languages.findGroovy()
println languages.findScala()
println languages.findJava()

def language2 = new LanguageList()
println language2.findGroovy()
