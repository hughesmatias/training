
class Operations {
    def sum() {
        {list ->
            list.inject(0) { total, num ->
                total + Integer.parseInt(num)
            }
        }
    }

    def prod() {
        {list ->
            list.inject(1) { total, num ->
                total * Integer.parseInt(num)
            }
        }
    }

    def methodMissing(String name, args) {
        println "no se encuentra implementada la operacion requerida."
    }
}

class Test {
    static visitor(g, List list) {
        g(list)
    }

    static void main (args) {
        if (args.size() == 2) {
            List list = args[0].substring(1,args[0].size()-1).split(",")
            def op = new Operations()
            def operacion = op."${args[1]}"()

            try {
                println visitor(operacion, list)
            } catch(Exception e) {
                //aca se debe manejar correctamente la excepcion
                println "Ocurrio una excepcion"
            }
        } else {
            println "Argumentos mal enviados. Lista y metodo"
        }
    }
}
