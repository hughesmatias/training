Groovy - Grails:

Groovy es un lenguaje, y grails es un framework.

Grails hace uso del lenguaje que es groovy, pero como groovy hace uso de la base de java, puede usar codigo JAVA.
Cuando decimos framework, significa que ya se disponen definidas varias funcionalidades
que estan disponibles para ser usadas.
Tambien podemos decir que grails la principal caracteristica es que sirve para realizar
aplicaciones web.


convention over configuration:
Es una convension base de ubicacion de los archivos de nuestra aplicacion grails.
Donde tendremos views, controllers. principales carpetas del patron MVC
(Modelo vista control).
config : configuracion de la aplicacion.
grails-app -> controller


Services: Son el lugar para establecer la logica de nuestra aplicacion, dejando
al controller el control del flujo.

Url Mapping: Establece una convencion de las urls de la aplicacion, aca se podra
establecer que controller y cual accion usar para una ruta especifica:

```
"/product"(controller: "product", action: "list")
```


###### My sql

Grails necesita una dependencia la cual se debe agregar en el build grandle

```
runtime 'mysql:mysql-connector-java:5.1.36'
```


#### constrains

Son las restricciones que se pueden establecer a las propiedades de un dominio.
```
static constrains {
  propiedad restricion:valor
}
```
ej: email email:true
la propiedad email es de tipo mail, y el true activa la propiedad.
ej: email black:flash


#### Controller

Maneja petisiones y crea respuestas.
Se pueden crear con el comando grails create-controller, lo alojara directamente
en la carpeta controllers, o sino desde el Intellij.

Por defecto el URLMapping usa el nombre del controller y las funciones definidas
por defecto para definir endpoints.
create controller :
```
grails create-controller book
```

Cada accion del controller es apuntado a una ruta de acceso.

Por defecto si tenes una sola accion es esa la tomada.
Sino sera index o list.
Se puede configurar eso en cada controller particular, definiendo cualquier otro
accion.

#### Scope
Dentro de un controller se disponen ciertas variables

params -> son parametros enviados.
request -> el request de la peticion.

hay otros mas.

#### Modelo y vista
El modelo es un mapa para que la vista sea renderizada.
Una manera de retorno es el de devolver una instancia.
```
def book = Book.get(1)

render ([books: book])
```

##### Seleccionando un modelo

Alternativamente se pueden devolver vistas con el modelo renderizado.
Una manera es hacer uso de las views GSP, donde uno puede mandarle informacion, un
modelo y que ese sea renderizado.

La manera es haciendo uso de:
render(view:"/list", model: favouritesBooks)
un path absoluto permitiria mayor posibilidad de compartir con otros modelos.
path = "/list/view"


Otra manera es haciendo uso del nameSpace para el nombre del view.

si uno especifica el nameSpace, y no el nombre del view, o si, se armara
el path en conjunto de ambos.

Entonces se pueden renderizar->
vistas especificas, textos con una coleccion, texto con encode y content type.

Y tambien se puede usar la clase MarkupBuilder para generar codigo HTML y ser
renderizado.

#### Redireccionamiento y encadenamiento

En las acciones de los controllers, se puede hacer uso del redirect para algunas
situaciones, (ej: control se session ).
redirigiendo a una accion del mismo controller.
`redirect(action: 'login')`
o de otro controller:
`redirect(controller: 'product', action: 'list')`

tambien de uri, o de urls.

redirect(uri: 'admin/list.html')
redirect(url: 'http://google.com')


#### Data binding

Los datos en los request son strings, y las propiedades de las clases dominio, muchas
veces no. Aca es donde se deberan parsear o castearlos a tipo objeto.

En un dominio properties contiene un map de las propiedades de una clase.

```
bindingNewPerson = [firstName: 'Matias', lastName: 'Perez']
Person {
  firstName
  lastName
}
def p = new Person()
p.properties = bindingNewPerson
```

El binding tambien puede ser de map de maps.

###### Union de request data a un modelo

realizar el save de un objeto, mandando al momento de hacer un new de tal objeto,
y parametrizando los parametros obtenido de una peticion, es una opcion directa
de crear el objeto para grabar.

```
params = [title: 'El diabo en la botella']
def save () {
  def book = new Book(params)
  book.save()
}
```

`/book/save?title=The%20Stand&author=Stephen%20King` esa seria la ruta solicitada.

Una cadena vacia es traducida en un null.
Si tenemos una restriccion que no acepta null, lanzara la excepcion.

##### data binding con varias clases
se pueden obtener por parametros varias clases dominios.

y tomarlos por separado

`/book/save?author.name=El Autor&book.title=El Titulo`

ejemplo de beans
https://docs.grails.org/latest/guide/theWebLayer.html#CustomFormattedConverters

`import org.springframework.context.ApplicationContext`


### URL Mapping

La convension por defecto de las urls son :
`/controller/action/id`
Esta se establece en el controller UrlMappings.groovy que se encuentra entre los
controllers.

Este solo contendra la simple propiedad mappings.

##### 8.4.1 Mappings para controllers y sus acciones:

Para crear una URL simple, se debe establecer que accion y que controller la respondera,
se establece la ruta con los parametros que va a necesitar.

`"/product"(controller: "product", action: "list")`

Se puede establecer
`"/product"(controller: "product")`
sin especificar la accion de list, o index, ya que seran vinculadas por defecto.

Si quieres asignar rutas especificas, se puede, agrupandolas.

```
group "/store", {
    group "/product", {
        "/$id"(controller:"product")
    }
}
```

Tambien se puede hacer un vinculo de dos, teniendo en cuenta que se desea redireccionar
a una accion, accediendo desde otra.
`"/hello"(uri: "/hello.dispatch")`

##### 8.4.2 Mapping a recursos REST

En grails desde la version 2.3 se pueden crear acciones bajo una convension, lo cual
permite crear urls bajo un resource y con eso ya alcanza para definir cada una
de las rutas.
Entonces si tenemos acciones definidas como index el cual debe retornar
una lista.

`"/books"(resources:'book')`

Los resources pueden excluir o incluir tales acciones.
Esto se puede especificar.

```
"/books"(resources:'book', excludes:['delete', 'update'])

"/books"(resources:'book', includes:['index', 'show'])
```


Tambien existe el recurso simple. el cual la ruta sera solo para uno y no muchos.

```
"/book"(resource:'book')
```
Esto descarta el recurso de ruta para index. osea para listar varios.

Tambien existen recursos anidados.
Esto genera rutas:
y ids por defectos.

```
"/books"(resources:'book') {
  "/authors"(resources:"author")
}
```
los libros del autor 1, tendra como nombre de parametro de autor. (id).

#### 8.4.3 Redireccion de urls
Se puede redireccionar accesos a urls.
Esto permite definir una ruta y te lleve a otra.

```
"/viewBooks"(redirect: '/books/list')
"/viewAuthors"(redirect: [controller: 'author', action: 'list'])
"/viewPublishers"(redirect: [controller: 'publisher', action: 'list', permanent: true])
```

##### 8.4.4 Envevido de variables

En las url se pueden disponer de variables.
Segun como sean enviadas, parametrizadas o en el cuerpo de la peticion sera como
se las recibira en el CONTROLLER.

si es parametrizada sera por params.
Si es en el body como request.

```
class ProductController {
     def index() { render params.id }
}
```

Se puede identificar tambien el formato de archivo solicitado.

/recibo/123/resumen.pdf
ese pdf puede ser espeficiado y luego recibido en el controller.
```
"/$controller/$action?/$id?(.$format)?"()
```

y luego en el controller:

`response.format`

#### 8.4.5 Mapeo de vistas

Se pueden establecer vistas para cada ruta.

```
static mappings = {
   "/help"(controller: "site", view: "help") // to a view for a controller
}
```

#### 8.4.6 Mapeo de vista segun error
```
static mappings = {
   "403"(view: "/errors/forbidden")
   "404"(view: "/errors/notFound")
   "500"(view: "/errors/serverError")
}
```

#### 8.4.7 Asterisco admitido
```
static mappings = {
    "/images/*.jpg"(controller: "image")
}
```
esto admite un comodin en la ruta.

?? para que sirve como obtengo la informacion.
