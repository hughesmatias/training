<?php
include '../scripts/Tren.php';
include '../scripts/LocomotoraDiesel.php';
include '../scripts/EmuElectrico.php';

function manejarTren(Tren $obj) {
  $obj->acelerar();
}

$electrico = new EmuElectrico();
$diesel = new LocomotoraDiesel();

manejarTren($diesel);
echo "<br>";
manejarTren($electrico);

?>
