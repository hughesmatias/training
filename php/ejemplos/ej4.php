<?php
  include '../scripts/Figura.php';
  include '../scripts/Rectangulo.php';
  include '../scripts/Circulo.php';

  $rectangulo = new Rectangulo();
  $circulo = new Circulo();

  echo "demostrando polimorfismo. el rectangulo va a tomar la clase del padre.";
  echo "<br>";
  class Ejecutar {
    function init (Figura $obj) {
      $obj->queSoy();
    }
  }
  $ejecutar = new Ejecutar();
  $ejecutar->init($rectangulo);
  echo "<br>";
  $ejecutar->init($circulo);

?>
