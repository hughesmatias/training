<?php
include '../scripts/Figura.php';
include '../scripts/Circulo.php';
include '../scripts/Rectangulo.php';

$circulo = new Circulo();
$circulo->dibujar();
$circulo->getTamanio();
echo "es un ejemplo de herencia y clase padre abstracta.<br>";

//esto deberia fallar ya que figura es abstracta
// $figura = new Figura();
// $figura->dibujar();

// Rectangulo
echo "Rectangulo";

$figura = new Rectangulo();
echo "<br>";
$figura->getTamanio();
echo "<br>";
$figura->getTamanio();
$figura->setTamanio(5);
echo "<br>";
$figura->getTamanio();

?>
