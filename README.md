# Sobrecarga

Es el poder declarar mas de una funcion bajo el mismo nombre, pero con distintos parametros. Los parametros son distintos si tienen distinto tipo o cantidad.

1. sumar(int unValor, int otroValor) { return unValor + otroValor; }*

2. sumar(string unValor, string otroValor) { return parseInt(unValor) + parseInt(otroValor);}*

Entonces es definir dos funciones con el mismo nombre pero que se diferencia por los parametros.

Existe una relacion directa con el tipado.

### Tipado fuerte:
Nosotros debemos definir el tipo de cada variable u objeto, esto se ve en JAVA
```
int i;
i = 3;
```
si ahora queremos ponerle un string a i nos devolvera un error el compilador, ya que i solo admitira tipo int.
### Tipado debil:
Los lenguajes debilmente tipados en cambio, no se debe pre definir el tipo de la variable ( no rotular con ningun tipo a una variable), solo le asignamos el valor que deseamos que tenga, y puede tener la flexibilidad de tener cualquier tipo luego.

Al crear una funcion, no puede asignarsele tipo a los parametros, entonces, al intentar crear una funcion con mismo nombre, esta se sobreescribira.

```
function getNombre(nombre) {
  console.log( nombre + “ es mi nombre”);
}
```

```
function getNombre(nombre, apellido) {
  console.log( nombre + “ “ + apellido + “ son mis nombres”);
}
```

Al intentar hacer uso de la primera, no se podra, ya que fue sobreescrita por la segunda.(arreglar)

Para simular la sobrecarga en javascript en funciones se hace uso de:
arguments - es un array con los parametros de la funcion.
arguments.length - devuelve la cantidad de parametros de la funcion.
y podemos usar la funcion typeOf() para saber el tipo.

#### Ejemplos:
```
function getNombre() {
  if (arguments.length === 1) {
    console.log(arguments[0] + “es mi nombre”);
  } else {
    var nombres;
    for (var i = 0; i < arguments.length; i++) {
      nombres +=” ” + arguments[i];
    }
    console.log(“estos son mis nombres” + nombres);
}
```
```
function suma(param1, param2) {
  if ((typeof(param1) === 'number') && (typeof(param2)=== 'number' )) {
    return param1 + param2
  }
  if ((typeof(param1) === 'string') && (typeof(param2)=== 'string')) {
    return parseInt(param1) + parseInt(param2)
  }
}
```

En php se puede simular parecido a javascript con las funcione funct_get_args() y funct_num_args(); y usar gettype() devuelve un string con el tipo de variable.


# Herencia

"La herencia es un mecanismo que permite la definición de una clase a partir de la definición de otra ya existente. La herencia permite compartir automáticamente métodos y datos entre clases, subclases y objetos.
La herencia está fuertemente ligada a la reutilización del código en la POO. Esto es, el código de cualquiera de las clases puede ser utilizado sin más que crear una clase derivada de ella, o bien una subclase."

La herencia es la transmision de codigo entre una clase y otra. Para eso se debera tener una clase padre e hija. La clase hija hereda todo lo de la clase padre (metodos y propiedades).
Mecanismo que elementos especificos incorporan metodos y propiedades de elementos mas generales.
Donde es posible especializar o extender funcionalidad de una clase, derivando una nueva clase.

Hay transitividad de una clase a otra super clase, a varios niveles.

Puede ser herencia simple o multiple.

En PHP, la herencia es implementada usando la palabra clave extends.
```
class Animal {
  function hablar () {
    echo “yo no hablo, soy un animal”;
  }
}
class Perro extends Animal {
   function hablar() {
     echo “no hablo soy un perro”;
   }
}
```
La clase hija hereda las propieades y comportamientos de la clase padre.

Las restricciones de la herencia se dan:
Desde los modificadores, que segun cual tenga le da limitaciones a los metodos o propiedades.

*si son public sera accedido por cualquiera en el script.
si es private sera solo accedido dentro de la clase definida.
protected solo pertenece a la clase esa y sus descendientes
final -> no se puede sobreescribir en sus descendientes.
abstract -> no se lo puede usar, solo heredar.
por defecto todo es public.*

Otra restriccion es si es clase abstract no se la puede instanciar.


### Metodo de clase - metodo de instancia:
los metodos de clase son los que no es necesario instanciar en un objeto, sino son directo sobre la clase.
En cambio los metodos de instancia se debera instanciar a un objeto de esta clase y luego hacer uso de ese metodo.
Ej:
Array es una clase en java que usamos y no la instanciamos, sino hacemos uso de metodos propios, al igual que Math class.


### Herencias multiples - simples:
Herencia simple pueden definir nuevas clases solamente a partir de una clase inicial mientras que herencia multiple indica que se pueden definir nuevas clases a partir de dos o más clases iniciales.

Java no permite herencias multiples.

### Clase abstracta

La clase abstracta se define con la palabra clave abstract class.
Si una clase tiene un metodo abstracto, esa clase debe ser abstracta.

Una clase que extiende de una clase abstracta debe definir los metodos que son abstractos.

### Interfaz

Se define con la palabra clave interface.
Lo que hace la interface es definir un contrato, estableciendo las condiciones bases.
Solo se define el encabezado de las metodos, no el funcionamiento. Obliga a quienes implementen la interface a crear los metodos y darle el funcionamiento. Los metodos en la interface tienen el modificador publico.

### Clase concreta
Las clases que pueden usarse para instanciar objetos se denominan clases concretas.


# Sobre Escritura

Sobreescritura, es el redefinir un metodo que existe en la clase padre.

class padre {
  public function imprimir() {
    echo “padre imprime”;
  }
}

class hijo extends padre {
  public function imprimir() {
    echo “imprime el hijo”;
  }
}
ahora bien si no tenemos el metodo imprimir en el hijo, y una clase de tipo hijo realiza el mensaje imprimir, se ejecutara el del padre.
en php para llamar el metodo del padre se hace uso de la palabra clave parent::imprimir();


# Polimorfimo
Objetos distintos entienden el mismo mensaje que puede o no reaccionar de manera diferente.
Es la relajacion del sistema de tipo, de manera que una referencia a una clase acepte direcciones de esa clase y de sus derivadas.
Aca se relaciona con herencia, al permitir heredar comportamientos y propiedades de su padre, dejara compreder los mismos mensajes.


¿como funciona?

Funciona al tener una funcion en una clase que hace uso del tipo padre de una jerarquia, y en la implementaicon hacer uso de alguno de los comportamientos, Permitir enviar, al hacer uso de esa clase, cualquier tipo de objeto que sea parte de la jerarquia (por la relajacion de la clase).

¿como podés asegurar que haya polimorfismo?
los objetos a usar el polimorfismo deben saber responder el mensaje el que se lo envian. Y tambien parte de la jerarquia, ya que uno va a usar el tipo padre para que se puedan entender si se envia una clase derivada.

Cual crees que son las ventajas?

Minimizar la cantidad de funciones, no es necesario repetir codigo de comportamientos de otros objetos que nos solucionan funcionalidades que tambien nuestro objeto quiera.

# HTTP
Es un protocolo de comunicacion y transferencia de datos.
Define una sintaxis y semantica para la comunicacion entre los clientes.
Es un protocolo sin estado ya que no guarda la info sobre conexiones anteriores.
Las aplicaciones frecuentemente necesitan mantener estado, y como http es sin estado, se hace uso de los cookies, que permiten mantener informacion almacenada del lado del cliente.
Esto permite generar sessiones (un dialogo entre computadoras intercambiando informacion).
Un protocolo es establecer reglas o normas que permiten la comunicacion entre maquinas, para el intercambio de mensajes. Estan acordados por las partes involucradas.  

Teniendo un cliente - servidor, lo primero que surge es la SOLICITUD desde cliente a servidor, estas se pueden componer de:

1. linea de solicitud: que tiene el metodo, la direccion URL, y la version del protocolo (HTTP 1.0)
2. los campos del encabezado de la solicitud:
lineas opcionales aportan info adicional ej: navegador, SO, etc.
3. el cuerpo de la solicitud:
lineas opcionales: permiten enviar datos por comando ejemplo usando el verbo POST el cual usamos en un formulario para enviar informacion.

Luego de esto el servidor realizara la decodificacion de la solicitud.
Luego de procesar se realiza la RESPUESTA.
Esta incluye:
1. linea de estado:
especifica la versión del protocolo utilizada, el estado de la solicitud en formato de un numero codigo y el significado.
2. encabezado de respuesta:
líneas opcionales que permiten aportar información adicional sobre la respuesta y/o el servidor.
3. cuerpo de la respuesta:
Contenido de la respuesta.
// averiguar si puede ser vacio el cuerpo. y que puede contener?


### Stateless
Un protocolo stateless es aquel que cada comunicacion la hace independiente sin relacion a una solicitud anterior. Las comunicacion se comportan de a pares (una solicitud y una respuesta). Estos protocolos sin estado, no requieren mantener datos de una sesion o datos algunos durante multiples peticiones.

// saco la ultima linea. pero investigo cuales son los protocolos que necesitan mantener estado.

### Requests con una session
Estando ya logeado, sin importar el proceso de logeo, en cada acceso a una nueva pagina lo primero que haria es verificar si se encuentra una session empezada.
En php, una forma era en cada pagina que se necesita estar logeado, como primer linea de codigo se verificaba la session, como minimo si estaba empezada.

### Servicio responde con error
No se obtendra la informacion o content type solicitado. Se podra identificar el tipo de error viendo el status de la respuesta.

#### Codigos de errores
200 OK  La solicitud se llevó a cabo de manera correcta
30x Redirección Estos códigos indican que el recurso ya no se encuentra en la ubicación especificada
40x Error debido al cliente Estos códigos indican que la solicitud es incorrecta
50x Error debido al servidor  Estos códigos indican que existe un error interno en el servidor

### Como puedo saber que tipo de dato es el que el servidor me esta mandando?
Con el cont-type del header de la respuesta.


### Los 4 verbos principales del protocolo HTTP

Los cuatro verbos principales en el protocolo HTTP son:
GET, POST, PUT y DELETE.
GET se usa la mayor parte del tiempo. Se usa para cualquier operación no destructiva, segura y que no causa efectos secundarios.

### Podria pedirle que me lo envie en algun otro formato?
Si tiene seteado en el Accept varios tipos, se establece que tipos seran aceptados para ser solicitados.
Accept: text/plain, text/html

#### Content-Type
Tipo de contenido del cuerpo de la solicitud.

#### Accept
Tipo de contenido aceptado por el cliente.


### Basic Authentication

Es un metodo diseñado para dar credenciales de usuario y password.
Es la forma basica para authentificar para aplicaciones web. No requiere uso de cookies, ni paginas de ingreso, tampoco de identificadores de sesion.

### Lenguajes de programación: Compilados - Interpretados
Lenguaje interpretado:
Existe un interprete que se encarga de analizar el codigo y traducirlo a medida que sea necesario, instruccion a instruccion,no se guarda esta traduccion a lenguaje de maquina.
Las variables son tipado dinamico, no es necesario definir el tipo en ningun momento. Tienen independencia de la plataforma donde sea ejecutado.

Lenguaje compilado:
El codigo fuente es compilado a un executable, entendible para una maquina de plataforma especifica. Pudiendo ejecutar cuantas veces se quiera, sin esperar el tiempo de compilacion.
Java es un caso especial que hace uso de una maquina virtual, que traduce el codigo fuente. Denominandolo compilado e interpretado.La maquina virtual permite ejecutar en cualquier maquina que tenga jvm, permitiendo ser multiplataforma.
-¿que es interpretado? y ¿que es compilado?
¿que es el proceso de compilacion?

# OSI

Division en capas de el proceso de transmision de informacion entre equipos informaticos.

7 capas donde se transmiten los datos:
1. Capa aplicacion
Ofrece acceder a los servicios de las demás capas y define los protocolos que utilizan las aplicaciones para intercambiar datos.
Uno no interactua de forma directa con los protocolos, sino a traves de las aplicaciones, que ocultan la complejidad.
Por ejemplo en una peticion web, para uno es transparente http 1.0.
[se concede la interface de acceso a la red]
ej un archivo de texto es subido a un servidor o enviado a una impresora.

Entonces se proporciona la interface para que las aplicaciones tengan acceso a la red.

2. capa presentacaion:
texto -> traducido idioma receptor. (clientes (origen y destino) se ponen de acuerdo con una sintaxis de comunicacion comun).
[los clientes se comunican para elegir la mejor sintaxis]

3. capa session.
se realiza conexion cliente emisor y receptor.
[se encarga de mantener el enlace entre los dos computadores que estén trasmitiendo archivos/ paquetes ]
asegura una comunicacion fluida.

4. capa transporte:
se elije el protocolo que sea confiable o no. para establecer la conexion  con el receptor.

5. capa de red:
se añade la direccion destino.
[La direccion de destino es agrega a los paquetes. Los protocolos hacen el seguimiento en esta capa para garantizar la confiabilidad.]

6. capa enlace de datos:
se convierte formato correcto para la transmision.

7. capa fisica:
el doc es pasado al cable de red.
Es la capa donde se transmiten los datos en forma de impulsos electricos.

# Programacion dinamica

Es la posibilidad de que una clase tenga una implementacion para metodos que nosotros no sabemos que dispone en RUNTIME.
Para esto se implementa un metodo denominado methodMissing -> este es invocado cuando otro metodo no puede ser encontrado.
Entonces, en una clase se puede disponer metodosque son invocados y no se los encuentra.
MethodMissing si esta definido -> es invocado y realiza lo definido.

def methodMissing (String name, args) {
  //se hace lo que se desea que ocurra cuando un methodo no es encontrado.
}

El objetivo con esto es que en el proximo momento que sea llamado este mismo metodo no definido, uno lo tenga implementado. Por esoen method missing se lo va a implementar.
Aca se lo puede implementar a este metodo, a la clase o a la instancia o ambos.

Groovy dispone una propiedad metaClass, que hace referencia a ExpandoMetaClass, que nos permite añadir, modificar metodos, propiedades y tambien constructores.

Class Book {
  String title
}

Book.metaClass.año = 1990
Esto tambien incorpora el getter y el setter para esa propiedad.
Ocurriria al modo contrario,
Book.metaClass.getAutor = "El autor"
Este genera el setter y la propiedad autor.


La principal ventaja es tener un codigo flexible, que puede adaptarse a cualquier cambio que uno pueda imaginar.

La principal desventaja es hacer que el código sea mas difícil de leer y más dificil de depurar, ya que la mayoria de las veces el código no esta alli todavia, al ser generarado en tiempo de ejecucion.

# Fases de vida de un programa:

Las etapas de un programa son desde la creacion, despliege y ejecucion, y las fases son tiempo de edicion, tiempo de compilacion, de distribucion, de instalacion, de acceso carga y tiempo de ejecucion.

No es necesario que tengan un orden estas fases. Cuando se modifica un programa, un developer necesita repetidamente editar, compilar, instalar y ejecutar para garantizar antes de ser distribuido.

Ej: al modificar un programa, el desarrollador necesita repetir varias veces editar, ejecutar e instalar, pero no distribuir el programa.

Fases:
- tiempo de edicion:
Es cuando se edita el codigo, creando nueva cracterisitas, refactorizando, o correguiendo errores. Suele ser por una persona, usando herramientas.

- tiempo de compilacion:
El codigo pasa por un compilador, es traducido a lenguaje de maquina. Este comprueba correcto uso de tipos.
El resultado es un ejecutable.

- tiempo de distribucion:
Transferir un programa a un usuario. las formas son: con un ejecutable o con el codigo fuente que seria en los casos de prog escritos en lenguajes interpretados. Y los medios de distribucion serian: memorias flash o descargas por internet.

- tiempo de instalacion:
es correr o ejecutar el ejecutable, para que se desencadene la instalacion de la aplicacion y lo necesario.

- tiempo de enlace:
Se da el enlace a librerias externas, que son requeridas para el funcionamiento del compilador.

- tiempo de carga:
El SO extrae al programa del disco y lo pasa a memoria ram para ejecutarlo.

- tiempo de ejecucion:
Es la fase de ejecucion que cuando la CPU ejecuta el codigo de maquina.


# JVM

La JVM es la maquina virtual de JAVA, nos permite ejecutar programas JAVA. Esta se encarga de de ejecutar bytecodes, estos son generados por el compilador de JAVA.

Java dispone de
1. JRE -> es un paquete de software que contiene lo requerido para correr un programa JAVA. Incluyendo la JVM y las librerias de clases de JAVA.

2. JDK -> es el conjunto de la JRE y las herramientas para desarrollar JAVA.

El bytecode no es el código máquina de ninguna computadora en particular, y puede por tanto ser portable entre diferentes arquitecturas. El bytecode es entonces interpretado, o ejecutado por una máquina virtual.

#### Tipos de JVM:
Java disponen maquinas virtuales de cada uno de los sistemas operativos LINUX, UNIX, WINDOWS, tambien Solaris. Pero tambien existen maquinas virtuales mas chicas y mas especificas, como para navegadores, o para los celulares.

1. HotSpot es la principal VM destacada. Es para maquinas de escritorio y servidores.
Mantenida y distribuida por ORACLE.
Ofrece buen rendimiento en tiempo de compilacion. (just-in-time compilation). Esta implementada en un principio en Smalltalk.

2. Bck2Brwsr — Pequeña JVM capaz de arrancar rápidamente y ejecutarse en el 100% de los navegadores modernos, incluyendo aquellos que no tienen soporte especial para Java.

Desde un .java con JAVAC (compilador de JAVA) se genera el bytecode que va a ser funcional para cualquier sistema o equipo que disponga una JVM.
La maquina virtual de Java se encarga de entender archivos escritos por nosotros, y generar a codigo navito del dispositivo final en bytecode.

Frase asociada a lo poderoso que es JAVA:
"escríbelo una vez, ejecútalo en cualquier parte"

### Proceso de compilacion .java -> binario .class

<!-- Mi programa .java es compilado por javac y me genera un .class. para ser interpretado en lenguaje de maquina.

Tenemos un editor: es el que sirve para escribir el codigo del programa. Puede ser un bloc de notas, o cualquier editor basico. al archivo se le debe anexar la extension .java

Luego el compilador:
Este toma el codigo .java y lo convierte en un formato comprendido por la maquina. El programa es ejecutado en la maquina de JAVA que esta dentro del paquete de desarrollo JDK, la maquina es JVM.

El JDK genero en el proceso de compilacion un .class, el cual el interprete de la JVM interpreta la salida. -->

La compilacion es un proceso de reducir el codigo fuente a una representacion intermedia portable y optimizable. Esto arranca con JAVAC, el compilador java. Cuando este codigo se ejecuta, la JVM lo traduce a codigo maquina nativo.

### Programa basico en JAVA

```
public class Basica {
   public static void main(String args[]){
     System.out.println("hola mundo");
   }
}
```

compilar con la JDK:
```
javac Basica.java
```

si esta bien escrito el codigo, nos genera el .class

que se ejecuta con la JVM:
```
java basica.class
```

#### JAR - WAR

Archivos .jar: Los archivos .jar contienen bibliotecas, recursos y archivos de accesorios como archivos de propiedades.
.war: El archivo war contiene la aplicación web que se puede implementar en cualquier contenedor servlet / jsp. El archivo .war contiene jsp, html, javascript y otros archivos necesarios para el desarrollo de aplicaciones web.

# Groovy

Que es?
Lenguaje orientado a objetos. Implementado sobre la plataforma de java. Es dinamico(no es necesario definir el tipo al declarar una variable).
Tambien es interpretado, es decir, al definir variables, no es necesario declarar el tipo de dato
de las mismas, no tiene un sintaxis estricta en el cuerpo
principal de los scripts; y todo lo que se escribe en Groovy es ejecutado por el interprete.
Ventaja:
Su sintaxis es compatible con Java, pero añade
funcionalidades y atajos que ahorran muchas líneas de
código
Cualquier clase/objeto Java es un clase /objeto Groovy.
Sintaxis:
1. No hace falta poner return en los métodos que
devuelven algo (se devuelve directamente lo que de la
última línea)
2. Los paréntesis al llamar a un método son opcionales.
3. El ';' al final de cada sentencia también es opcional
4. No existen tipos primitivos: los objetos y métodos se
crean con def.
5. Closures: Bloques de código reusables (similar a cómo
se define una función anónima en JavaScript).

Sintaxis parecida a JAVA pero simplificada, se puede usar toda la api de Java.

Un programa JAVA se puede ejecutar en la plataforma de GROOVY.
Groovy generar bytecodes similares a Java y se ejecuta en JVM.

### Groovy vs Java:
Modificador de acceso:
Java -> si no  se lo especifica es privado para campos, métodos o clases.
Groovy -> las cosas son por defecto públicas.

metodos getter y setter:
Java -> debe proporcionar métodos getters y setters para los campos.
Groovy -> generan automáticamente.

groovy es superconjunto de java:
un programa Java funcionará bien en el entorno Groovy.
un programa groovy puede que no funciona dependiendo que use.

para imprimir en consola:
Java -> System.out.println
Groovy -> println

Permite sustitucion de variables en comillas dobles:
Groovy -> println("Good morning $name !")
Java -> se debe concatenar "hola "+ $name +" , como estaS?";

### Tipo:
Java-> se debe especificar el tipo.
Groovy -> es opcional, para metodos o variables se pueden definir con la palabra reservada def.

con los getter y setter disponibles:
Java -> se usa el punto para acceder.
Groovy -> hacer uso de los setters.

punto y coma:
java al final de cada instruccion hace uso de ;
groovy -> es opcional

Java necesita un metodo inicial para ser ejecutado.
Groovy no, automaticamente se envuelve en una clase script para poder ser ejecutado en la JVM.

Arreglos:
Java -> {'a', 'b'}
Groovy -> ['a', 'b']

### Funciones de alto orden

Aquellas funciones que reciben como parametro una funcion o retornan una funcion.

El map, seria el collect.
Se aplica una funcion a una lista.

El filter seria el findAll o find (ese para obtener solo el primero que cumpla una condicion).
Filtrando por una condicion obteniendo una coleccion.
```
[123,23,12].findAll {
    it<100
}
```
obteniendo un arreglo con los menores a 100. [23, 12]




### Proceso de compilacion

groovyc es la herramienta para compilar un archivo .groovy. Este se encarga de generar el bytecode, al igual que javac.
groovyc MyClass.groovy
genera MyClass.class

# Closures

Un closure en Groovy es tres cosas:
- Un bloque de código que puede acceder a variables en el ámbito donde se declara.
- Una función que puede tomar argumentos y siempre devuelve un resultado (puede ser nulo)
- Un objeto que tiene propiedades y métodos con y sin efectos secundarios

Un closure es una forma reducida de definir un método, podemos pasarle parámetros, puede modificar el valor de una variable.
Enfocado a procesar listas o colecciones de objetos.
por ejemplo podemos iterar a través de sus elementos y aplicarle un closure a ellos.

Si no deseamos parámetros de entrada, se debe omitir la lista de parámetros y el separador “->”. Por ejemplo

```
imprime={ print "hola a todos"}
```

y luego para hacer uso del mismo debe hacerse
```
imprimir.call()
```
otra forma:
```
imprimir()
```


### Closures: Class:

También podemos crear una subclase de la clase Closure para implementar un closure. Entonces usamos una instancia de la nueva clase de cierre dondequiera que se pueda usar un closure.
Como clase creamos una nueva clase, la cual extiende de Closure, e implementamos doCall(), este metodo es el que se llama cuando se instancie la clase.
Este metodo puede recibir parametros.

#### Notacion:

```
class Sumar extends Closure {
  Sumar () {
    super(null)
  }

  def doCall(Integer valor1, Integer valor2) {
    valor1 + valor2
  }
}

[2,5,6].collect(new Sumar(it,10))
```


#### DSL:

dominio ->
es una accion que se puede hacer dentro de una aplicacion.
ej: registrarse como medico en togetherhood.
ej: busquedas en togetherhood.

Entonces el dominio es todo lo que está relacionado, en este caso, la creación de usuarios o la busqueda.

Y el DSL seria:
El lenguaje que defino para realizar las busquedas.
¿que es?
Son expresiones cortas de la programacion destinada a un tipo especifico


puede servir para otros casos, digo bueno lo exporto a todos mis trabajos, ejemplo el ¿por que ?

la idea es que si ahora tu lenguaje se usa para todos los nombres de todas las aplicaciones ya no es mas espécifico

Ya no es mas para el dominio de elegir nombres en el registro de togetherhood.

------
Groovy permite omitir paréntesis alrededor de los argumentos de una llamada de método para las instrucciones de nivel superior. La característica "cadena de comandos" amplía esto al permitirnos encadenar tales llamadas de método libres de paréntesis, no requiriendo paréntesis alrededor de argumentos ni puntos entre las llamadas encadenadas.
```
turn(left).then(right)
turn left then right
```

También es posible utilizar métodos en la cadena que no toman argumentos, pero en ese caso, los paréntesis son necesarios

```
select(all).unique().from(names)
select all unique() from names
```

Llamar a una propiedad de forma directa

```
take(3).cookies
take 3 cookies
```


#### Inversion de control
La programacion antes era lineal y monolitica.
es el modo inverso a la ejecucion de programas de forma tradicional. donde secuencialmente va ejecutando
linea a linea. Uno define secuencialmente paso a paso que se va a hacer, llamandose a funciones.
En la inversion de control, uno establece que se hara a partir de solicitudes de datos concretos.

Ej:
Se requieren usuarios con un apellido especifico.
Entonces uno va a predeterminar que cuando se pidan usuarios por el campo apellido,
se devolveran datos de usuarios que coincida el apellido que se solicita.

A partir de la Injeccion de dependencias uno provee

#### Injeccion de dependencias
##### http://codecriticon.com/inyeccion-de-dependencias/
##### http://codecriticon.com/dependencia-inyeccion-dummies/

Es un patron de diseño donde se provee a una clase un objecto y no que la clase cree una instancia
de el objecto.

```
class AutoSinDI {
  motor = new Motor();

  Auto() {
    motor.prender()
  }
}

class Auto {

  Auto(Object motor) {
    motor = motor;
  }
}

a1 = new AutoSinDI()

// ejemplo con DI

motor = new Motor()
auto = new Auto(motor)

```


# Spring - Beans

Un beans es un componente de software el cual es creado para ser reusado. para evitar
programar tediosos componentes una y otra vez.
Existen para ahorrar tiempo de programacion.
Un beans puede representar un boton, un panel contenedor, o un simple campo de texto.

Los beans deben tener propidades privadas, metodos set y get para las propiedades, y
un constructor publico.
https://es.wikipedia.org/wiki/Bean

IOC
En spring el modulo Core es ContenedorIoC, Contenedor IoC se encargarca de
instanciar los objetos de nuestro sistema, denominados beans, y asignarle sus
dependencias. En la configuracion deberemos decir donde estan los beans ubicados.


Beans: como se declara.
En cualquier parte de nuestro proyecto, lo importante es el @Service que le servira
a Spring identificarlo como comoponente que esta disponible en cualquier parte del
proyecto.

```
@Service("sillyService")
public class SillyClass {
  private String string = "Silly string.";

  public String getString() {
    return string;
  }

  public void setString(String string) {
    this.string = string;
  }
}
```



http://www.programacionj2ee.com/spring-bean/


http://blog.hcosta.info/fundamentos-de-spring-los-beans/


# GORM

#### Introduccion

es un framework con implementaciones ya disponibles para acceder de forma
facil a datos en DB.

La implementacion original de GORM esta basada en HIBERNATE ORM.
HIBERNATE es una herramienta de mapeo objeto relacional de JAVA.

Existen implementaciones para bd relacional es y NOSQL.


#### Guia Rapida
Para crear dominios:

desde grails. el comando
`create-domain-class NombreDeDominio`
ej
`create-domain-class Person`
Directamente lo ubicara al archivo .groovy en domain

```
class Person {

}
```

se pueden agregar propiedades ahi adentro.
ej
```
class Person {
  String firstName
  String lastName
}
```

#### Crud Basic
Create - Read - Update - DELETE
las 4 operaciones disponibles.

Para crear una clase domino se puede hacer uso del mappeo de objeto.
```
def p = new Person(firstName: 'Matias', lastName: 'Perez')
p.save()
```
de esta manera se estaria creando el objeto, save realiza la persistencia en la
base de datos.

Read:
Gorm incoropora a los objetos un id transparentemente, con ellos podremos acceder.
```
def p2 = Person.get(3)
p2.lastName
```
si no existe retorna null con get().
`Person.load(2)`
el load si o si debes garantizarte que existe ese id.

`Person.read(2)`
podras leer objetos que estan en modo solo lectura. con el get cuando dispones
de bd.

Update:
Debes tener una instancia, y luego realizar un save()

```
def per = Person.get(4)
per.lastName = 'Lopez'
per.save()
```

Delete:
Tambien a partir de una instancia, se realiza el delete()

`per.delete()`


#### Configuracion

La configuracion de nuestra aplicacion grails, se realiza desde el archivo
application.yml


Mapping y constrain:
Se pueden configurar por defecto en el archivo de configuracion application.yml.
```
grails.gorm.default.constraints = {
    '*'(nullable: true, size: 1..20)
}
```

```
grails.gorm.default.mapping = {
        cache true
        id generator:'uuid'
}
```

o se puede especificar en cada uno de los dominios.

```
class Person {
    String login
    String password
    String email
    Integer age

    static constraints = {
        login size: 5..15, blank: false, unique: true
        password size: 5..15, blank: false
        email email: true, blank: false
        age min: 18
    }

    static mapping = {
        version false
        autoTimestamp false
    }
}
```
establecer sino para un dominio el nombre con el que va a estar en la BD o sino
el nombre de una columna en la tabla representada.
```
    static mapping = {
        table 'people'
        firstName column: 'First_Name'
    }
```


#### Modelando dominios con GORM
```
class Book {
    String title
    Date releaseDate
    String ISBN
}
```
Ese dominio se vera representado en una tabla con el nombre book.
A traves de las constrains podemos tambien definir si queremos fields de HasMany
con el id separado por _
`author_id.`


##### Asociacion

1 a 1
```
Face {
  Nose nose
}

Nose {

}
```
Esto seria solo con una direccion Una cara tiene una nariz.
deberia ser de ambos lados esto
En Nose agregar el belongsTo face.
```
class Nose {
    static belongsTo = [face:Face]
}
```


#### Control de concurrencia optista

Esto es aplicado a sistemas transaccionales.
Este asume que multiples transacciones se puedan dar al mismo tiempo sin interferir.
Las transacciones hacen usos de recursos, la clave es que no los bloqueen.
Pero antes de hacer el commit, se debe verificar que esos datos no hayan cambiado por
otra transaccion.
Si no hay conflicto se completa la etapa de transaccion, sino se debera hacer un
roll back y se la puede reiniciar, ya que porque hay modificaciones en el registro
que esa transacciones iba a modificar.

Bloqueo pesimista:
Los datos son bloqueados previos a su modificacion para que nadie mas los modifique.
Una vez esto, se puede commitear los cambios. Se elimina el bloqueo automaticamente.
Alguien intenta bloquear los datos, no podra, debera esperar hasta terminar la
transaccion en curso.
Esta tiene dos problemas:
El bloqueo y el deadlock:

en el caso del bloqueo todos los que quieran realizar una modificacion de un registro
bloqueado deberan esperar a que termina la operacion o que muera.

el deadlock se da cuando hay una espera indefinida de mismos registros entre dos
usuarios.
Ejemplo:
2 chicos quieren jugar al arco y flecha, uno toma el arco el otro la flecha. Fijate
que uno depende del otro pero ninguno va a liberarlo hasta que uno libere lo que tiene que
necesita el otro.

el deadlock es denominado bloqueo mutuo.

bloqueo optimista:
No bloquea los registros a modificar.
Asume que no cambiaran desde que son leidos.
Es necesario un control de concurrencia, para que exista concistencia entre el registro
leido y modificado, osea que ninguna transaccion haya modifcado durante la leida.
El procesimiento de actualizacion concistente es muy sencillo:
se lo lee al registro y se lo actualiza.


https://unpocodejava.wordpress.com/2011/01/10/tecnicas-de-bloqueo-sobre-base-de-datos-bloqueo-pesimista-y-bloqueo-optimista/

http://www.teknoplof.com/2010/08/06/concurrencia-optimista-en-bases-de-datos/


#### Excepciones

Existen excepciones que son chequeables y las que no.

Las chequeables heredan de la clase RuntimeException.
No es necesario capturarlas con el try catch, pero estas cortaran el flujo de ejecucion.


ejemplo ArithmeticException. -> Esta se lanza cuando se divide por 0.

Las no chequeabes son son las que heredan de la clase Exception. Estas son disparadas
al momento e compilarse nuestro codigo, y en ese momento se frenara la compilacion
del codigo.

Estas excepciones pueden darse cuando se buscan recursos externos, archivos, conexiones con
DB que no se puede establecer tal conexion por algun motivo.


Y la sentencia throw se usa para lanzar excepciones.

http://labojava.blogspot.com.ar/2012/05/excepciones.html


#### Transacinonal
##### commit / rollback

Las transaciones se dividen en 3 partes:
la etapa donde comienza. BEGIN.
La etapa donde avisa que fue con exito, ya que fue completada. COMMIT.
La etapa donde se dio un fallo. ROLLBACK.
En este ultimo se vuelve al punto anterior de empezar la transaccion.

Si una transaccion lanza una excepcion del tipo RuntimeException esta generara un
rollback.


##### readonly

Son aquellas transaciones que mediante una anotacion se las define como transaciones de
solo lectura.

`@Transactional(readonly=true)`
Y a continuacion el codigo sera solo para realizar lecturas y no modificaciones en
nuestra base de datos.

##### propagation required

Es la que viene por defecto, así que no es necesaria especificarla.
Si existe transacción la aprovecha y sino la crea


##### REQUIRES_NEW 
Abre una transacción nueva y pone en suspenso la anterior.
Una vez el método marcado como REQUIRES_NEW termina se vuelve a la transacción
anterior.


##### lastUpdated createdDate y version

Estos campos permiten saber si ubieron cambios, esto permitira ver y comparar al momento
al realizar una persistencia optima (sin bloqueo).

Version esta por defecto creado en cada clase Dominio.

LastUpdate y createdDate son opcionales.

##### Flush

Al realizar una transaccion con el save con flush en true, lo que va a generar es
que en la misma base de datos se estaria dejando la persistencia.
Esto permitira que si se da una excepcion, no afectara al cambio que uno queria
realizar.


#### Lock - pesimistic:

Se puede realizar con un .get(id) y luego el .lock() a esa instancia.



#### GORM - relaciones- Asociacion:

##### One to one

Este es el simple ejemplo de tener dos clases dominio, Customer y Address, donde
un customer tiene una direccion.

CUSTOMER  - Sring firstName, ADDRESS Address
ADDRESS - String city, String street

Esto generara en CUSTOMER un field con el id de del address.

Esto es con una sola direccion.

Se puede realizar bi direccionalmente, con CUSTOMER customer en ADDRESS.

Algo mas interesante es que una clase pertenezca a otra, y esto puede generar decencadenamiento
en cascada para actualizar o borrar los registros que sean pertenecientes.

Ejemplo una identidad pertenezca a un customer.

IDENTITY String email, String password, y tener un belongsTo = Customer.

Esto generara que si se borra la identidad, el customer sera borrado.


#### One to many

Una clase esta asociada a muchas instancias de otra clase.
Ej:
un cliente puede tener muchas facturas.
Y cada factura tener ordenes de compra para cada uno de los reglones de una factura.

Si una factura es borrada cada uno de las ordenes de compra seran borradas.

Cliente: String nombre, hasMany= [facturas: Factura]

Factura: hasMany= [ordenesDeCompra: OrdenCompra ]
OrdenCompra: String codigo, Float precio, belongsTo= Factura

Esto provoca tablas intermedias entre cliente factura y factura ordene de compra


##### Many to many

N N para ambos lados, donde hay una una clase A que tiene varias instancias de B, pero
tambien B tiene muchas instancias de A.

Una cancion es de varios artistas.
un artista tiene varias canciones.
A su vez una cancion le pertenece a un artista.

Cancion nombreCancion , hasMany [ cantantes = cantante ], belongsTo: cantante
Cantante nombreCantante , hasMany [ canciones = cancion ]


Esto de que una cancion pertenece aun cantante, puede generar, que si ese cancion
no es mas del cantante, esa cancion ya no pertenecera a ningun cantantes.


##### Composicion:

Componer clases, donde se unen las propiedaes y se genera una sola tabla modelada.
```
class Customer {
   String firstName
   String lastName
   Address billing
   Address shipping
   Identity ident
   static embedded = ['billing','shipping','ident']
}

class Address {
   String street
   String city
}

class Identity {
   String email
   String password
}
```

##### Relaciones de herencia:

Tener clases que heredan de otras clases, genera una sola tabla.

Class A extends B {
  firstName
}

Class B extends C {
  lastName
}

Class C {
  fullName
}

Generar una tabla con el nombre de la primera clase con las 3 propieades.

#### Mapping de la clase

Se puede establecer en el mapping { } el nombre de la tabla, el como denominar especificamente
ciertas propieades.
